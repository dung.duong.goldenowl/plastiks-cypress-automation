describe('CreateCollection', () => {
    it('Connects with Metamask', () => {
        cy.viewport(1920,1080)
        cy.visit({url: 'https://stage-app.plastiks.io/', failOnStatusCode: false});
        cy.wait(10000);
        cy.contains('Log in').click();
        cy.wait(1000);
        cy.get('#connect-wallet-trigger > div').click();
        cy.get("#connect-wallet__connect-wallet > div > div > a:nth-child(2) > div").click();
        cy.wait(5000);
        cy.acceptMetamaskAccess().should("be.true");
        cy.wait(10000);
        cy.get('span').should('have.class', 'curBalance')
        cy.log('login ');

        cy.visit('https://stage-app.plastiks.io/nft/nft_collections/new')
        
        cy.wait(7000)

        // cy.get('.header__nav-item.show > [href="javascript:void(0);"]').click()
        // cy.wait(1000)
        // cy.get('.header__nav-item.show > [href="javascript:void(0);"]').click()

        //Input data into form create collection

        cy.get('#nft_collection_name').type("Cypress collection 1")
        cy.get('#nft_collection_description').type("Cypress collection test")

        cy.wait(5000)
         
        cy.get('.select-collection__logo').attachFile({
            filePath: '1.jpg',
            //encoding: 'utf-8'
        }, {
            //force: true,
            subjectType: 'drag-n-drop'
        });

        cy.wait(5000)
        
        cy.get('.select-collection__cover').attachFile({
            filePath: '1.jpg',
            //encoding: 'utf-8'
        }, {
            //force: true,
            subjectType: 'drag-n-drop'
        });

        cy.get('.triggerNftCollectionValidation').click()
        cy.wait(7000)
        cy.confirmMetamaskDataSignatureRequest();
        cy.wait(5000)

    })
})




