describe('CreatePRG', () => {
    it('CreatePRG', () => {
        cy.viewport(1920,1080)
        cy.visit({url: 'https://stage-app.plastiks.io/', failOnStatusCode: false});
        cy.wait(10000);
        cy.contains('Log in').click();
        cy.wait(1000);
        cy.get('#connect-wallet-trigger > div').click();
        cy.get("#connect-wallet__connect-wallet > div > div > a:nth-child(2) > div").click();
        cy.wait(5000);
        cy.acceptMetamaskAccess().should("be.true");
        cy.wait(10000);
        cy.get('span').should('have.class', 'curBalance')
        cy.log('login ');

        cy.wait(3000);

        // cy.get('.header__nav > :nth-child(4) > [href="javascript:void(0);"]').click()
        // cy.wait(1000)
        // cy.get('.header__nav-item.show > .dropdown-menu > :nth-child(2) > .header__nav-link').click()
        cy.visit({url: 'https://stage-app.plastiks.io/collections/new?role=recycler'});
        cy.wait(3000);
        // input data to form create PRG
        cy.get('#groupFileInvoice').attachFile({
            filePath: '1.jpg',
            //encoding: 'utf-8'
        }, {
            force: true,
            subjectType: 'drag-n-drop'
        });
        cy.get('#properties').type("2000")
        cy.get('#idOfInvoice').type("2000kg cypress")
        cy.get('#dateOfRecycling').dblclick()
        cy.get('#dateOfRecycling').type("2023-07-07")
        //country
        cy.get('#selectCountryId > .select2 > .selection > .select2-selection').click()
        cy.wait(1000)
        cy.xpath("//span[contains(text(),'Antarctica')]").click()
        //cy.contains("Antarctica").click({force: true})
        cy.wait(2000)
        // city
        // cy.get('#selectCityId > .select2 > .selection > .select2-selection').click()
        // cy.wait(1000)
        // cy.contains("Balkh").click({force: true})
        

        cy.get('.create-item').click()

        cy.wait(15000)

        cy.confirmMetamaskDataSignatureRequest();

        cy.wait(7000)

        cy.confirmMetamaskDataSignatureRequest();

        cy.wait(5000)
        
        cy.get('.purchase-success-prg__link--view col-6').click()
        cy.wait(5000)
    })
})