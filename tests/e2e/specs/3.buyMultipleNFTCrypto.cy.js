describe('buy Multiple nft Crypto', () => {
    it('buyMultipleNFT', () => {
        cy.viewport(1920,1080)
        cy.visit({url: 'https://stage-app.plastiks.io/', failOnStatusCode: false});
        cy.wait(10000);
        cy.contains('Log in').click();
        cy.wait(1000);
        cy.get('#connect-wallet-trigger > div').click();
        cy.get("#connect-wallet__connect-wallet > div > div > a:nth-child(2) > div").click();
        cy.wait(5000);
        cy.acceptMetamaskAccess().should("be.true");
        cy.wait(10000);
        cy.get('span').should('have.class', 'curBalance')
        cy.log('login ');

        cy.wait(5000);
        //cy.get(".collections-buy-button").click()
        cy.visit({url:'https://stage-app.plastiks.io/nft/search?tab=arts'});
        cy.wait(7000)
        cy.get('.collections-buy-button').first().click()
        cy.wait(10000)
        //cy.get('#nft_amount').type("3")
        // cy.wait(9000);
        cy.get(".sufficient-funds").click()
        cy.wait(10000)
        cy.confirmMetamaskPermissionToSpend();

        cy.wait(10000)

        if(cy.url().should('include','/purchase_success'))
        {
            cy.get("#btn-view-profile-owned").click()
        }else
        { 
            cy.confirmMetamaskTransaction();
            cy.wait(10000);
            cy.get("#btn-view-profile-owned").click()
        }
        cy.wait(5000)

    })
})