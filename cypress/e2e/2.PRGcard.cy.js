describe('BuyPRGCard', () => {
    it('PRGcard', () => {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // cy.viewport(1920,1080)
        // cy.visit({url: 'https://stage-app.plastiks.io/', failOnStatusCode: false});
        // cy.wait(10000);
        // cy.contains('Log in').click();
        // cy.wait(1000);
        // cy.get('#connect-wallet-trigger > div').click();
        // cy.get("#connect-wallet__connect-wallet > div > div > a:nth-child(2) > div").click();
        // cy.wait(5000);
        // cy.acceptMetamaskAccess().should("be.true");
        // cy.wait(10000);
        // cy.get('span').should('have.class', 'curBalance')
        // cy.log('login ');

        //cy.get(".collections-buy-button").click()
        cy.visit('https://stage-app.plastiks.io/users/amy-ma-1234-123-12345689/recovery_entity_profile?action=show&controller=users&id=amy-ma-1234-123-12345689')
        cy.wait(3000);
        cy.get(':nth-child(5) > #triggerStartCheckout').click()
        cy.wait(3000);
        cy.get('.payment-button-options > .pay-button').click()
        cy.wait(3000)
        //Input card
        cy.get('#stripe-email').type("dungduong@mailinator.com")
        cy.get('#stripe-card-holder').type("Test")

        getStripeField({
            iframeSelector: 'iframe[title="Secure card number input frame"]', 
            fieldSelector: 'div.CardNumberField-input-wrapper'
        })
        .type('4242424242424242')

        getStripeField({
            iframeSelector: '[title="Secure expiration date input frame"]', 
            fieldSelector: 'input[name="exp-date"]'
          })
        .type('4242')

        getStripeField({
            iframeSelector: '[title="Secure CVC input frame"]', 
            fieldSelector: 'input[name="cvc"]'
          })
        .type('424')
          
        cy.wait(1000);

        cy.get('#stripe-pay-prg').click()

        cy.wait(3000);
        function getStripeField({iframeSelector, fieldSelector}, attempts = 0) {
            Cypress.log({displayName: 'getCardField', message: `${fieldSelector}: ${attempts}`})
          
            if (attempts > 50) throw new Error('too many attempts')
          
            return cy.get(iframeSelector, {timeout:10_000, log:false})
                .eq(0, {log:false})
                .its('0.contentDocument')
                .find('body', {log:false})
                .then(body => {
                const stripeField = body.find(fieldSelector)
                if (!stripeField.length) {
                    return cy.wait(300, {log:false})
                    .then(() => {
                        getStripeField({iframeSelector, fieldSelector}, ++attempts)
                    })
                } else {
                    return cy.wrap(stripeField)
                }
            })
        }
        cy.wait(12000)
        cy.url().should('include','purchase_success')  
        cy.wait(5000)
    })
})